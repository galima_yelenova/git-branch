﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tictactoe_
{
    public partial class Form1 : Form
    {
        MyButton[,] btns = new MyButton[3, 3];
        tictactoe t = new tictactoe();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    btns[i, j] = new MyButton(i, j);
                    btns[i, j].Size = new Size(75,75);
                    btns[i, j].Location = new Point(j * 75, i * 75);
                    btns[i, j].Click += new System.EventHandler(btn_click);
                    this.Controls.Add(btns[i, j]);
                }
            }
        }
        private void btn_click(object sender, EventArgs e)
        {
            MyButton b = sender as MyButton;
            b.Enabled = false;
            int m = t.move(b.x, b.y);
            if (m == 0)
            {
                b.Text = 'x'.ToString();
            }
            if (m == 1)
            {
                b.Text = 'o'.ToString();
            }
            int w = t.win();
            if (w == 1)
            {
                MessageBox.Show("Первый игрок выиграл");
                t.restart();
                for (int i = 0; i < 3; i++)
                    for (int j = 0; j < 3; j++)
                    {
                        btns[i, j].Text = "";
                        btns[i, j].Enabled = true;
                    }
            }
            if (w == 2)
            {
                MessageBox.Show("Второй игрок выиграл");
                t.restart();
                for (int i = 0; i < 3; i++)
                    for (int j = 0; j < 3; j++)
                    {
                        btns[i, j].Text = "";
                        btns[i, j].Enabled = true;
                    }
            }
            if (w == 3)
            {
                MessageBox.Show("Ничья");
                t.restart();
                for (int i = 0; i < 3; i++)
                    for (int j = 0; j < 3; j++)
                    {
                        btns[i, j].Text = "";
                        btns[i, j].Enabled = true;
                    }
            }
        }
    }
}
