﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace interface_sample
{
    class Program
    {
        // Создаем 2 интерфейса, описывающих абстрактные методы
        // суммы и прозведения и операций sqr
        public interface IOperation
        {
            // Определяем набор абстрактных методов
            int Sum(); 
            int Product();
        }
        public interface ISqr
        {
            int Sqr(int x);
        }
        // Данный класс реализует интерфейс IOperation
        class A : IOperation
        {
            int My_x, My_y;

            public int x
            {
                set { My_x = value; }
                get { return My_x; }
            }

            public int y
            {
                set { My_y = value; }
                get { return My_y; }
            }

            public A() { }
            public A(int _x, int _y)
            {
                x = _x;
                y = _y;
            }
            // Реализуем методы интерфейса
            public virtual int Sum()
            {
                return x + y; 
            }
            public virtual int Product()
            {
                return x * y;
            }
            // В данном классе можно реализовать собственные методы
            public virtual void rewrite()
            {
                Console.WriteLine("Переменная х: {0} \n Переменная у: {1}", x, y);
            }
            // Данный класс унаследован от класса А, и при этом
            // реализует интерфейс ISqr
            class Ab : A, ISqr
            {
                public int Sqr(int x)
                {
                    return x * x;
                }
            }
        }
        static void Main(string[] args)
        {
            A obj = new A (_x: 5, _y: 6);
            Console.WriteLine("obj:");
            obj.rewrite();
            Console.WriteLine("{0} + {1} = {2}", obj.x, obj.y, obj.Sum());
            Console.WriteLine("{0} * {1} = {2}", obj.x, obj.y, obj.Product());
            Console.ReadKey();
        }
    }
}
