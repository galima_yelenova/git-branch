﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tictactoe_
{
    class MyButton : Button
    {
        public int x, y;
        public MyButton(int _x, int _y)
        {
            x = _x;
            y = _y;
        }
    }
}
